#!/usr/bin/env bash

# directories
mkdir -p src/pug/component src/scss/component src/js build orig

# md
touch README.md TODO.md note.md

# entry points
touch src/pug/index.pug src/scss/style.scss