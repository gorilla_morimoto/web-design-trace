# Gigazineの記事ページ

2018-06-06

## 参考ページ

https://gigazine.net/news/20180605-marshmallow-test-rich-kids/

## 目的

CSS設計の経験を積む

## 計画

- [x] pugで構造を作る
- [x] ダミーテキストを流す
- [x] レイアウトをcssで実装
- [ ] 各Layoutごとに作っていく
  - [x] recent
  - [x] header
  - [x] main
  - [x] nav
  - [x] footer
- [x] 画像を作る
- [x] レイアウトを調整する

## 振り返り

- `c-h1`みたいに、クラス名にタグ名を入れるのはやめる
  - 課題のFBであった
  - タグが変わったときにクラス名も道連れになる

- まだデザインの再現で手一杯でメディアクエリまで手が回らない
