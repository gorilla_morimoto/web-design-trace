# Webデザインの模写

## 構成

- domain/: どこのサイトか
  - view/: どの画面か
    - asset/: 画像など
    - build/: html, css
    - src/: pug, scss
    - capture.png
    - README.md

## メモ

- `private/`は`gitignore`されます

## 実績

- はてな
  - ログイン画面

- GitLab
  - ログイン画面

- Gigazine
  - 記事ページ

